import { ImagemPost } from "./imagemPost";


export class PostGaleria {
    id: number = 0;
    descricao: string = '';
    action: string = '';
    totalComentarios: number = 0;
    tipoPostagem: number = 0;
    totalCurtidas: number = 0;
    visualizacoes: number = 0;
    dataHoraPublicacao: Date = new Date;
    imagens: ImagemPost[] = [];
    titulo: string = '';
    usuarioId: number = 0;
    usuarioNome: string = '';
}
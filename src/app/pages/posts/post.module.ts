import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { PostsComponent } from './posts.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { PostDetalhesComponent } from './post-detalhes/post-detalhes.component';
import { FilterByDescription } from './section-posts/filter-by-decriprion.pipe';
import { SectionPostsComponent } from './section-posts/section-posts.component';
import { SectionArtigosRecentesComponent } from '../principal/section-artigos-recentes/section-artigos-recentes.component';
import { PostListaComponent } from './section-posts/post-lista/post-lista.component';
import { PrincipalModule } from '../principal/principal.module';


@NgModule({
  declarations: [
    PostDetalhesComponent,
    FilterByDescription,
    SectionPostsComponent,
    PostsComponent,
    SectionArtigosRecentesComponent,
    PostListaComponent
  ],

  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    NgxDropzoneModule,
    PrincipalModule
  ]
})
export class PostModule{ }

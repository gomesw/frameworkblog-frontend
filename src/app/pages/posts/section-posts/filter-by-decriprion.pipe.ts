import { Pipe, PipeTransform } from '@angular/core';
import { Post } from 'src/app/models/post';

@Pipe({ name: 'filterByDescription'})
export class FilterByDescription implements PipeTransform {
    transform(posts: Post[], descriptionQuery:string) {
       descriptionQuery = descriptionQuery.trim().toLowerCase();

       if(descriptionQuery){
           console.log("não tem filtro");
           return posts.filter(post =>
            post.categoriaNome.toLowerCase().includes(descriptionQuery)
        );
       }
       else{
           console.log("teste test e" );
           return posts;
       }


    }
}
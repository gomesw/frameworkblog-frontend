import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { API_CONFIG } from 'src/app/config/api.config';
import { Post } from 'src/app/models/post';
import { CategoriaService } from 'src/app/services/categoria.service';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-lista',
  templateUrl: './post-lista.component.html',
  styleUrls: ['./post-lista.component.css']
})
export class PostListaComponent implements OnInit {

  
  filter: string = 'flavio';
  posts$!: Observable<Post[]>;
  posts: Post[] = [];
  paginacao: any = '';
  paginaAtual = 1;
  paginaLast = false;
  page = 0;
  paginaTotal = 0;
  url: string = '';

  constructor(
    private postService: PostsService, 
    private categoriaService: CategoriaService) {
    this.url = API_CONFIG.baseUrl;
   }

  ngOnInit(): void {
    this.getListPost();
  }

  ngAfterViewInit() {
    this.getListPost();
  }


  

  getPostsParametro(parametro:string) {
    this.posts$ = this.postService.getPosts(3, this.page,parametro);

    this.posts$.subscribe((res : any) => {
      this.posts = res['content'];
      
      if(this.posts.length > 0) this.posts.forEach(post => {
        this.categoriaService.getCategoria(post.categoriaId).subscribe(response => {
          if(response) post.categoriaNome = response.nome;
        });
      });
      this.paginacao = res;
      this.paginaLast = this.paginacao.last;
      this.paginaAtual = this.paginacao.number + 1;
      this.paginaTotal = this.paginacao.totalPages;
      console.log(this.paginacao);
     });
  }

  
  getListPost() {
    console.log(this.page);
    this.posts$ = this.postService.getPosts(3, this.page,'');
    this.posts$.subscribe((res : any) => {
     this.posts = res['content'];
     
     if(this.posts.length > 0) this.posts.forEach(post => {
       this.categoriaService.getCategoria(post.categoriaId).subscribe(response => {
         if(response) post.categoriaNome = response.nome;
       });
     });
     this.paginacao = res;
     this.paginaLast = this.paginacao.last;
     this.paginaAtual = this.paginacao.number + 1;
     this.paginaTotal = this.paginacao.totalPages;
     console.log(this.paginacao);
    });
  }

  retornarPagina() {
    if( this.paginaAtual > 1) {
      this.page--;
      this.getListPost();
    }
  }

  avancarPagina() {
    if(!this.paginaLast){
      this.page++;
      this.getListPost();
    }
  }

}

